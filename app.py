# -*- coding: utf-8 -*-
import web
import json
import datetime
from urllib2 import Request, urlopen

prefix = '/~czarnem1/apps/webservice'

urls = (
	prefix + '/note', 'Note',
	prefix + '/note/(.+)', 'Note',
	prefix + '/category/(.+)/notes', 'CategoryNotes',
	prefix + '/category', 'Category',
	prefix + '/category/(.+)', 'Category',
	prefix + '/tag', 'Tag',
	prefix + '/tag/(.+)/notes', 'TagNotes',
	prefix + '(.*)', 'Default'
)

app = web.application(urls, globals())
wsgi = app.wsgifunc()

db = [{"note_id": 0, "title": "bkbk", "content": "Coś", "category_id": 0, "tags": ["cos1", "cos2"], "create_time": "2014-01-18 23:23"}]
db.append({"note_id": 1, "title": "dddd", "content": "nkdbakd", "category_id": 1, "tags": ["sadas", "cos2"]})
categories = [{"category_id": 0, "category_name": "none"}, {"category_id": 1, "category_name": "dsada"}]
note_counter = 2
category_counter = 2

class Note:
    def GET(self, note_id=None):
        if note_id is None:
            note_list = []
            for note in db:
                note_list.append(note)
            return json.dumps(note_list)

        location = self.find_note_location(note_id)
        if location is not None:
            return json.dumps(db[location])
        else:
            print "Nie znaleziono notatki o id %s" % note_id
            raise web.notfound("Nie znaleziono notatki o id %s" % note_id)


    def POST(self):
        global note_counter
        data = web.data()
        try:
            note = json.loads(data)
            note['title']
            note['category_id']
            note['tags']
            note['content']
        except ValueError, KeyError:
            print "Niepoprawny format notatki"
            print data
            raise web.badrequest()

        tags = note['tags']
        tags.strip()
        if tags != '':
            note['tags'] = tags.split(' ')

        note['note_id'] = note_counter
        note_counter += 1

        now = datetime.datetime.now()
        create_time = now.strftime('%Y-%m-%d %H:%M')
        note['create_time'] = create_time
        note['edit_time'] = None

        if Category.find_category_location(note['category_id']) is None:
            print "Podana kategoria notatki nie istnieje"
            raise web.badrequest("Podana kategoria notatki nie istnieje")


        db.append(note)
        return json.dumps({"message": "Dodano nowa notatke"})

    def PUT(self, note_id):
        data = web.data()
        try:
            note = json.loads(data)
            note['title']
            note['category_id']
            note['tags']
            note['content']
        except ValueError, KeyError:
            print "Niepoprawny format notatki"
            print note
            raise web.badrequest("Niepoprawny format notatki" + note)

        location = self.find_note_location(note_id)
        if location is not None:
            note['note_id'] = int(note_id)
            note['create_time'] = db[location]['create_time']
            now = datetime.datetime.now()
            edit_time = now.strftime('%Y-%m-%d %H:%M')
            note['edit_time'] = edit_time

            tags = note['tags']
            tags.strip()
            if tags != '':
                note['tags'] = tags.split(' ')
            
            db[location] = note
            return json.dumps({"message": "Zmodyfikowano notatke %s" % note_id})
        else:
            print "Nie znaleziono notatki %s" % note_id
            raise web.notfound("Nie znaleziono notatki %s" % note_id)

    def DELETE(self, note_id):
        location = self.find_note_location(note_id)
        if location is not None:
            del db[location]
            return json.dumps({"message": "Usunieto notatke %s" % note_id})
        else:
            print "Nie znaleziono notatki o id %s" % note_id
            raise web.notfound("Nie znaleziono notatki o id %s" % note_id)


    def find_note_location(self, note_id):
        for note in db:
            if note['note_id'] == int(note_id):
                return db.index(note)
        return None

class Category:
    def GET(self, category_id=None):
        if category_id is None:
            return json.dumps(categories)

        location = self.find_category_location(category_id)
        if location is not None:
            return json.dumps(categories[location])
        else:
            print "Nie znaleziono kategorii o id %s" % category_id
            raise web.notfound("Nie znaleziono kategorii o id %s" % category_id)

    def POST(self):
        global category_counter
        data = web.data()
        try:
            category = json.loads(data)
            category['category_name']
        except KeyError:
            print "Niepoprawny format kategorii"
            print category
            raise web.badrequest("Niepoprawny format kategorii")

        if self.exists(category['category_name']):
            print "Kategoria o podanej nazwie już istnieje"
            raise web.badrequest("Kategoria o podanej nazwie już istnieje")

        category['category_id'] = category_counter
        category_counter += 1

        categories.append(category)
        return json.dumps({"message": "Dodano nowa kategorie"})

    def DELETE(self, category_id):
        if int(category_id) == 0:
            raise web.forbidden()
        location = self.find_category_location(category_id)
        if location is not None:
            category_name = categories[location]['category_name']
            del categories[location]
            self.update_categories(int(category_id), 0)
            return json.dumps({"message": "Usunieto kategorie %s" % category_name})
        else:
            print "Nie znaleziono kategorii o id %s" % category_id
            raise web.notfound("Nie znaleziono kategorii o id %s" % category_id)

    def PUT(self, category_id):
        if int(category_id) == 0:
            raise web.forbidden
        data = web.data()
        new_name = json.loads(data)
        location = self.find_category_location(category_id)
        if location is not None:
            if self.exists(new_name['category_name']):
                print "Kategoria o podanej nazwie już istnieje"
                raise web.badrequest("Kategoria o podanej nazwie już istnieje")

            else:
                old_name = categories[location]['category_name']
                categories[location]['category_name'] = new_name['category_name']
                return json.dumps({"message": "Zmieniono nazwe kategorii %s na %s" % (old_name, new_name['category_name'])})
        else:
            print "Nie znaleziono kategorii o id %s" % category_id
            raise web.notfound("Nie znaleziono kategorii o id %s" % category_id)

    def update_categories(self, old_id, new_id):
        for note in db:
            if note['category_id'] == old_id:
                note['category_id'] = new_id;
        return
    
    @staticmethod
    def find_category_location(category_id):
        for category in categories:
            if category['category_id'] == int(category_id):
                return categories.index(category)
        return None

    def exists(self, category_name):
        for category in categories:
            if category['category_name'] == category_name:
                return True
        return False

class CategoryNotes:
    def GET(self, category_id):
        note_list = []
        for note in db:
            if note['category_id'] == int(category_id):
                note_list.append(note)
        return json.dumps(note_list)
    
class Tag:
    def GET(self):
        tag_list = []
        for note in db:
            for tag in note['tags']:
                if tag != None and tag not in tag_list:
                    tag_list.append(tag)
        return json.dumps({"tag_names": tag_list})

class TagNotes:
    def GET(self, tag_name):
        note_list = []
        for note in db:
            for tag in note['tags']:
                if tag == tag_name:
                    note_list.append(note)
                    break
        return json.dumps(note_list)

class Default:
    def GET(self, *args):
        raise web.notfound()

if __name__ == '__main__':
    app.run()
